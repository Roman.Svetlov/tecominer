unit TCME;

interface
uses crt;
type
  block = record
    name: string;
    id: byte;
    symbol: char;
    symbol_drop: char;
    color: byte;
    text_color: byte;
    passability: boolean;
    stability: byte;
    condition: byte; 
    health: integer;
    hardness: integer;
  end;

var
  world: array [,,] of block;

var
  player_coordinate: array [,] of integer;//��������� - ������ 0 ��� 3-� ��������� ������, ������ 1 ��� 3-� ��������� ���

procedure init_array(width, height, depth: integer); forward;

procedure print_arr(display_mode: string; width, height: integer); forward;

procedure keyboard(); forward;

procedure generation_map; forward;

procedure var_init; forward;

procedure preservation; forward;

procedure block_physics; forward;

procedure loading; forward;

implementation

var
  width, height, depth: integer;
  width_print, height_print: integer;
  corrective_width, corrective_height, corrective_depth: integer;
  hand_mode: string;
  action_radius: integer;
  types_of_blocks: array of block;
  corrective_first_bool: boolean;
  auto_jump: boolean;
  branch_hand_mode: string;
  inventory_tape: array of block;
  block_in_hand: block;

procedure switching_hand_mode; forward;

procedure switching_jump_mode; forward;

procedure left_jump; forward;

procedure right_jump; forward;

procedure climbing_down; forward;

procedure climbing_up; forward;

procedure left_move; forward;

procedure right_move; forward;

procedure building(key: integer); forward;

procedure destroing(key: integer); forward;

function set_block_color(symbol: string): byte; forward;

procedure switching_branch_hand_mode; forward;

procedure player_move(x_delta, y_delta: integer); forward;

procedure inventory; forward;

procedure coordinate_correction_up; forward;

procedure player_physics; forward;

procedure coordinate_correction_down; forward;

procedure coordinate_correction_left; forward;

procedure coordinate_correction_right; forward;

procedure close_game_and_save; forward;

procedure walk(key: char); forward;

procedure init_array(width, height, depth: integer);
begin
  setlength(world, width, height, depth);
end;

procedure print_arr(display_mode: string; width, height: integer);
var
  bufer: string;
  symbol_in_line: integer;
  symbol_repeated: block;
begin
  width_print := width;
  height_print := height;
  clrscr;
  bufer := '';
  if corrective_first_bool = false then
  begin
    corrective_width := player_coordinate[0, 0] - (width_print div 2 + 1);
    corrective_height := player_coordinate[0, 1] - (height_print div 2 + 1);
    corrective_first_bool := true;
  end;
  if ((corrective_width >= 0 ) and (length(world, 1) - corrective_width >= width)
  and (corrective_height >= 0 ) and (length(world, 0) - corrective_height >= height)) then
    if display_mode = 'text' then 
    begin
      for line: integer := 0 to height - 1 do
      begin
        for column: integer := 0 to width - 1 do
          bufer += world[line + corrective_height, column + corrective_width, corrective_depth].symbol;
        bufer += #10;
      end;
      write(bufer);
    end
    else if display_mode = 'color' then 
    begin
      
      if ((corrective_width >= 0 ) and (length(world, 1) - corrective_width >= width)
      and (corrective_height >= 0 ) and (length(world, 0) - corrective_height >= height)) then
        for line: integer := 0 to height - 1 do
        begin
          while symbol_in_line < width do
          begin
            symbol_repeated := world[line + corrective_height, symbol_in_line + corrective_width, corrective_depth];
            
            while 
            ((symbol_in_line < width)
            and 
               (world[line + corrective_height, symbol_in_line + corrective_width, corrective_depth] = symbol_repeated) 
                ) 
                do
            begin
              //if symbol_repeated.condition > 0 then 
              //bufer += symbol_repeated.symbol else
              //bufer += symbol_repeated.symbol_drop;
              
              if symbol_repeated.condition > 0 then  bufer+=' '
              else bufer += symbol_repeated.symbol_drop;
              
              symbol_in_line += 1;
            end;
            textbackground(set_block_color(symbol_repeated.symbol));
            write(bufer + bufer);
            bufer := '';
            
          end;
          writeln;
          symbol_in_line := 0;
          
        end;
      
      if ((corrective_width >= 0 ) and (length(world, 1) - corrective_width >= width)
      and (corrective_height >= 0 ) and (length(world, 0) - corrective_height >= height)) then
        for line: integer := 0 to height - 1 do
          for column: integer := 0 to width - 1 do 
            if ((line + corrective_height = player_coordinate[0, 1] ) and (column + corrective_width = player_coordinate[0, 0] )) then 
            begin
              gotoxy((column) * 2 + 1, (line + 1));
              textcolor(set_block_color(world[player_coordinate[0, 1], player_coordinate[0, 0], corrective_depth].symbol)); 
              symbol_in_line += 1;textbackground(0);write('\/');
            end
            else
            if ((line + corrective_height = player_coordinate[1, 1] ) and (column + corrective_width = player_coordinate[1, 0] )) then 
            begin
              gotoxy((column) * 2 + 1, (line + 1));
              textcolor(set_block_color(world[player_coordinate[1, 1], player_coordinate[1, 0], corrective_depth].symbol));  
              symbol_in_line += 1;textbackground(0);write('/\');
            end;
      
      
    end;
  textcolor(7);
  textbackground(0);
  gotoxy(1, height_print + 3);write('xp' + player_coordinate[0, 0].ToString + 'yp' + player_coordinate[0, 1].ToString + 
    'xc' + corrective_width + 'yc' + corrective_height +
    ';' + 'auto_jump:' + auto_jump.ToString + ';' + 'hand_mode:' + hand_mode.ToString + ';' + 'action_radius:' + action_radius +
    ';' + 'branch_hand_mode:' + branch_hand_mode + ';' + 'block_in_hand:|' + block_in_hand.symbol + '|');
  
  gotoxy(width_print * 2 - inventory_tape.Length * 2, height_print + 1);
  for i: integer := 0 to inventory_tape.Length - 1 do write('|' + inventory_tape[i].symbol);write('|');
  
  gotoxy(1, 1);
  gotoxy(1, 1);
  textcolor(0);
  textbackground(7);
  write('');
end;

procedure keyboard;
var
  key: char;
begin
  key := readkey;
  case key of 
    #35: begin close_game_and_save; end;
    'w', '�': begin walk(key); end;
    'a', '�': begin walk(key); end;
    's', '�': begin walk(key); end;
    'd', '�': begin walk(key); end;
    'q', '�': begin walk(key); end;
    'e', '�': begin walk(key); end;
    '5': begin switching_hand_mode; end;
    ',', '.': begin switching_branch_hand_mode; end;
    'u', '�': begin switching_jump_mode; end;
    '+': begin if action_radius < 2 then action_radius += 1; end;
    '-': begin if action_radius > 1 then action_radius -= 1; end;
    'c', '�': begin inventory; end;
    '1': 
      begin
        case hand_mode of 
          'construction': building(1); 
          'destruction': destroing(1);
        end; 
      end;
    '2': 
      begin
        case hand_mode of 
          'construction': building(2); 
          'destruction': destroing(2);
        end; 
      end;
    '3': 
      begin
        case hand_mode of 
          'construction': building(3); 
          'destruction': destroing(3);
        end; 
      end;
    '4': 
      begin
        case hand_mode of 
          'construction': building(4); 
          'destruction': destroing(4);
        end; 
      end;
    '6': 
      begin
        case hand_mode of 
          'construction': building(6); 
          'destruction': destroing(6);
        end; 
      end;
    '7': 
      begin
        case hand_mode of 
          'construction': building(7); 
          'destruction': destroing(7);
        end; 
      end;
    '8': 
      begin
        case hand_mode of 
          'construction': building(8); 
          'destruction': destroing(8);
        end;
      end;
    '9': 
      begin
        case hand_mode of 
          'construction': building(9); 
          'destruction': destroing(9);
        end; 
      end;
  
  end;
  player_physics;
  for i: integer := 0 to 6 do block_physics;
end;

procedure var_init;
begin
  
  hidecursor;
  if ((hand_mode <> 'construction') and (hand_mode <> 'destruction')) then hand_mode := 'destruction';
  if ((branch_hand_mode <> 'top') and (branch_hand_mode <> 'bottom') and (branch_hand_mode <> 'middle')) then branch_hand_mode := 'middle';
  begin
    setlength(types_of_blocks, 256);
    types_of_blocks[0].symbol := 'w';types_of_blocks[0].passability := true; 
    types_of_blocks[1].symbol := 's';types_of_blocks[1].passability := false;
    types_of_blocks[2].symbol := 'd';types_of_blocks[2].passability := false;
    types_of_blocks[3].symbol := 'c';types_of_blocks[3].passability := false;
    types_of_blocks[4].symbol := ' ';types_of_blocks[4].passability := true;
    types_of_blocks[5].symbol := 't';types_of_blocks[5].passability := false;
    types_of_blocks[6].symbol := 'l';types_of_blocks[6].passability := true;
    types_of_blocks[7].symbol := '+';types_of_blocks[7].passability := true;
    
    types_of_blocks[0].stability := 2;//3 - ������� �����
    types_of_blocks[1].stability := 4;
    types_of_blocks[2].stability := 5;
    types_of_blocks[3].stability := 7;
    types_of_blocks[4].stability := 0;
    types_of_blocks[5].stability := 6;
    types_of_blocks[6].stability := 1;
    types_of_blocks[7].stability := 0;
    
    types_of_blocks[0].color := 11;
    types_of_blocks[1].color := 14;
    types_of_blocks[2].color := 6;
    types_of_blocks[3].color := 8;
    types_of_blocks[4].color := 7;
    types_of_blocks[5].color := 4;
    types_of_blocks[6].color := 2;
    types_of_blocks[7].color := 7;
    
    types_of_blocks[0].hardness := maxint;
    types_of_blocks[1].hardness := 1;
    types_of_blocks[2].hardness := 1;
    types_of_blocks[3].hardness := 3;
    types_of_blocks[4].hardness := maxint;
    types_of_blocks[5].hardness := 2;
    types_of_blocks[6].hardness := 1;
    types_of_blocks[7].hardness := 0;
    
    types_of_blocks[0].health := maxint;
    types_of_blocks[1].health := 1;
    types_of_blocks[2].health := 1;
    types_of_blocks[3].health := 3;
    types_of_blocks[4].health := maxint;
    types_of_blocks[5].health := 2;
    types_of_blocks[6].health := 1;
    types_of_blocks[7].health := 0;
    
    types_of_blocks[0].condition := 2;//w
    types_of_blocks[1].condition := 1;//s
    types_of_blocks[2].condition := 1;//d
    types_of_blocks[3].condition := 1;//c
    types_of_blocks[4].condition := 0;//' '
    types_of_blocks[5].condition := 1;//t
    types_of_blocks[6].condition := 1;//l
    types_of_blocks[7].condition := 0;
    
    types_of_blocks[0].symbol_drop := ',';//w
    types_of_blocks[1].symbol_drop := ',';//s
    types_of_blocks[2].symbol_drop := ',';//d
    types_of_blocks[3].symbol_drop := ',';//c
    types_of_blocks[4].symbol_drop := ' ';//' '
    types_of_blocks[5].symbol_drop := ',';//t
    types_of_blocks[6].symbol_drop := ',';//l
    types_of_blocks[7].symbol_drop := ',';
  end;
  
  setlength(inventory_tape, 10);
  begin
    inventory_tape[0] := types_of_blocks[3];
    inventory_tape[1] := types_of_blocks[2];
    inventory_tape[2] := types_of_blocks[1];
    inventory_tape[3] := types_of_blocks[0];
    inventory_tape[4] := types_of_blocks[5];
    inventory_tape[5] := types_of_blocks[6];
    inventory_tape[6] := types_of_blocks[4];
  end;
  setlength(player_coordinate, 2, 3);//([0,x] - head [1,x] - legs [x,(0,1,2)] = x,y,z;
  
  if ((action_radius <> 1) and (action_radius <> 2)) then action_radius := 1;
  
  player_coordinate[0, 0] := 600;
  player_coordinate[0, 1] := 277;
  player_coordinate[0, 2] := 0;
  
  player_move(0, 0);
  
  block_in_hand := types_of_blocks[3];
  
  auto_jump := true;
  
end;

procedure close_game_and_save;
begin
  preservation;
  halt;  
end;

procedure switching_hand_mode;
begin
  case hand_mode of
    'construction': hand_mode := 'destruction';
    'destruction': hand_mode := 'construction';
  end;
end;

procedure switching_branch_hand_mode;
begin
  case branch_hand_mode of
    'top': branch_hand_mode := 'middle';
    'middle': branch_hand_mode := 'bottom';
    'bottom': branch_hand_mode := 'top';
  end;
end;

procedure switching_jump_mode;
begin
  case auto_jump of
    true: auto_jump := false;
    false: auto_jump := true;
  end;
end;

procedure right_jump;
var
  jump_was_bool_local: boolean := false;
begin
  if ((player_coordinate[1, 0] < length(world, 1) - 1) and (player_coordinate[0, 1] > 1)) then 
  begin
    if ((world[player_coordinate[0, 1], player_coordinate[0, 0] + 1, 0].passability = false)
    and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 1, 0].stability >= 3)
    
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].stability < 3)
    and (world[player_coordinate[0, 1] - 2, player_coordinate[0, 0], 0].stability < 3)
    
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 1, 0].stability < 3)
    and (world[player_coordinate[0, 1] - 2, player_coordinate[0, 0] + 1, 0].stability < 3)
    
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].passability = true)
    and (world[player_coordinate[0, 1] - 2, player_coordinate[0, 0], 0].passability = true)
    
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 1, 0].passability = true)
    and (world[player_coordinate[0, 1] - 2, player_coordinate[0, 0] + 1, 0].passability = true))
    then 
    begin
      player_move(1, -2); 
      coordinate_correction_right;
      coordinate_correction_up; 
      coordinate_correction_up; 
      jump_was_bool_local := true; 
    end;
  end;
  if ((player_coordinate[1, 0] < length(world, 1) - 2) and (player_coordinate[0, 1] > 0) and (jump_was_bool_local = false)) then 
  begin
    if ((world[player_coordinate[1, 1], player_coordinate[1, 0] + 2, 0].passability = false)
    and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 2, 0].stability >= 3)
    
    and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 1, 0].stability < 3)
    and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 1, 0].passability = true)
    
    and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 1, 0].stability < 3)
    and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 1, 0].passability = true)
    
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 1, 0].stability < 3)
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 1, 0].passability = true)
    
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].stability < 3)
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].passability = true)
    
    and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 2, 0].stability < 3)
    and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 2, 0].passability = true)
    
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 2, 0].stability < 3)
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 2, 0].passability = true))
    then 
    begin
      player_move(2, -1); 
      coordinate_correction_right;
      coordinate_correction_right;
      coordinate_correction_up; 
      jump_was_bool_local := true; 
    end;
  end;
  
  if ((player_coordinate[1, 0] < length(world, 1) - 3) 
  and (player_coordinate[0, 1] > 0) 
  and (jump_was_bool_local = false)
  and (player_coordinate[1, 1] < length(world, 0) - 1)) then
  begin
    if ((world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] + 3, 0].passability = false)
    and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] + 3, 0].stability >= 3)
    //
    and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] + 2, 0].stability < 3)
    and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] + 2, 0].passability = true)
    
    and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] + 1, 0].stability < 3)
    and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] + 1, 0].passability = true)
    //
    and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 3, 0].stability < 3)
    and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 3, 0].passability = true)
    
    and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 3, 0].stability < 3)
    and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 3, 0].passability = true)
    //
    and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 2, 0].stability < 3)
    and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 2, 0].passability = true)
    
    and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 2, 0].stability < 3)
    and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 2, 0].passability = true)
    //
    and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 1, 0].stability < 3)
    and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 1, 0].passability = true)
    
    and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 1, 0].stability < 3)
    and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 1, 0].passability = true)
    //
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].stability < 3)
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].passability = true)
    
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 1, 0].stability < 3)
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 1, 0].passability = true)
    
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 2, 0].stability < 3)
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 2, 0].passability = true)
    
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 3, 0].stability < 3)
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 3, 0].passability = true))
    then 
    begin
      player_move(3, 0); 
      coordinate_correction_right;
      coordinate_correction_right;
      coordinate_correction_right;
      jump_was_bool_local := true; 
    end;
    
    if ((player_coordinate[1, 0] < length(world, 1) - 2) and (player_coordinate[0, 1] > 0) and (jump_was_bool_local = false)) then 
    begin
      if ((world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] + 2, 0].passability = false)
      and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] + 2, 0].stability >= 3)
      //
      and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] + 1, 0].stability < 3)
      and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] + 1, 0].passability = true)
      //
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 2, 0].stability < 3)
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 2, 0].passability = true)
      
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 2, 0].stability < 3)
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 2, 0].passability = true)
      //
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 1, 0].stability < 3)
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 1, 0].passability = true)
      
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 1, 0].stability < 3)
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 1, 0].passability = true)
      //
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].stability < 3)
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].passability = true)
      
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 1, 0].stability < 3)
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 1, 0].passability = true)
      
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 2, 0].stability < 3)
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 2, 0].passability = true))
      then 
      begin
        player_move(2, 0); 
        coordinate_correction_right;
        coordinate_correction_right;
        jump_was_bool_local := true; 
      end;
    end;
    
    if ((player_coordinate[1, 0] < length(world, 1) - 2) and (player_coordinate[1, 1] < length(world, 0) - 2) 
    and (player_coordinate[0, 1] > 0) and (jump_was_bool_local = false)) then
    begin//������ ���� ������ ����������� ����� �� ��� ������ �, �� ����.
      if ((world[player_coordinate[1, 1] + 2, player_coordinate[1, 0] + 2, 0].passability = false)
      and (world[player_coordinate[1, 1] + 2, player_coordinate[1, 0] + 2, 0].stability >= 3)
      //
      and (world[player_coordinate[1, 1] + 2, player_coordinate[1, 0] + 1, 0].passability = true)
      and (world[player_coordinate[1, 1] + 2, player_coordinate[1, 0] + 1, 0].stability < 3)
     
      and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] + 1, 0].stability < 3)
      and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] + 1, 0].passability = true)
      //
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 2, 0].stability < 3)
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 2, 0].passability = true)
      
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 2, 0].stability < 3)
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 2, 0].passability = true)
      //
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 1, 0].stability < 3)
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 1, 0].passability = true)
      
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 1, 0].stability < 3)
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 1, 0].passability = true)
      //
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].stability < 3)
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].passability = true)
      
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 1, 0].stability < 3)
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 1, 0].passability = true)
      
      and (world[player_coordinate[0, 1] + 1, player_coordinate[0, 0] + 2, 0].stability < 3)
      and (world[player_coordinate[0, 1] + 1, player_coordinate[0, 0] + 2, 0].passability = true)
      
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 2, 0].stability < 3)
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 2, 0].passability = true))
      then 
      begin
        player_move(2, 1); 
        coordinate_correction_right;
        coordinate_correction_right;
        coordinate_correction_down;
        jump_was_bool_local := true; 
      end;
    end;
    
    if ((player_coordinate[1, 0] < length(world, 1) - 2) and (player_coordinate[1, 1] < length(world, 0) - 2) 
    and (player_coordinate[0, 1] > 0) and (jump_was_bool_local = false)) then
    begin//������ ���� ������ ����������� ����� �� ��� ������ �, �� ����.
      if ((world[player_coordinate[1, 1] + 3, player_coordinate[1, 0] + 2, 0].passability = false)
      and (world[player_coordinate[1, 1] + 3, player_coordinate[1, 0] + 2, 0].stability >= 3)
      //
      and (world[player_coordinate[1, 1] + 2, player_coordinate[1, 0] + 1, 0].passability = true)
      and (world[player_coordinate[1, 1] + 2, player_coordinate[1, 0] + 1, 0].stability < 3)
     
      and (world[player_coordinate[1, 1] + 3, player_coordinate[1, 0] + 1, 0].passability = true)
      and (world[player_coordinate[1, 1] + 3, player_coordinate[1, 0] + 1, 0].stability < 3)   
      
      and (world[player_coordinate[1, 1] + 2, player_coordinate[1, 0] + 2, 0].passability = true)
      and (world[player_coordinate[1, 1] + 2, player_coordinate[1, 0] + 2, 0].stability < 3)
     
      and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] + 1, 0].stability < 3)
      and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] + 1, 0].passability = true)
      //
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 2, 0].stability < 3)
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 2, 0].passability = true)
      
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 2, 0].stability < 3)
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 2, 0].passability = true)
      //
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 1, 0].stability < 3)
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 1, 0].passability = true)
      
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 1, 0].stability < 3)
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 1, 0].passability = true)
      //
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].stability < 3)
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].passability = true)
      
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 1, 0].stability < 3)
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 1, 0].passability = true)
      
      and (world[player_coordinate[0, 1] + 1, player_coordinate[0, 0] + 2, 0].stability < 3)
      and (world[player_coordinate[0, 1] + 1, player_coordinate[0, 0] + 2, 0].passability = true)
      
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 2, 0].stability < 3)
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 2, 0].passability = true))
      then 
      begin
        player_move(2, 2); 
        coordinate_correction_right;
        coordinate_correction_right;
        coordinate_correction_down;
        coordinate_correction_down;
        jump_was_bool_local := true; 
      end;
    end;
    
    if ((player_coordinate[1, 0] < length(world, 1) - 2) and (player_coordinate[1, 1] < length(world, 0) - 2) 
    and (player_coordinate[0, 1] > 0) and (jump_was_bool_local = false)) then
    begin//������ ���� ������ ����������� ����� �� ��� ������ �, �� ����.
      if ((world[player_coordinate[1, 1] + 2, player_coordinate[1, 0] + 3, 0].passability = false)
      and (world[player_coordinate[1, 1] + 2, player_coordinate[1, 0] + 3, 0].stability >= 3)//
      //�������
      and (world[player_coordinate[1, 1] - 1, player_coordinate[1, 0] + 1, 0].passability = true)
      and (world[player_coordinate[1, 1] - 1, player_coordinate[1, 0] + 1, 0].stability < 3)
     
      and (world[player_coordinate[1, 1] - 1, player_coordinate[1, 0] + 2, 0].passability = true)
      and (world[player_coordinate[1, 1] - 1, player_coordinate[1, 0] + 2, 0].stability < 3)   
      
      and (world[player_coordinate[1, 1] - 1, player_coordinate[1, 0] + 3, 0].passability = true)
      and (world[player_coordinate[1, 1] - 1, player_coordinate[1, 0] + 3, 0].stability < 3)
      //������ �� �������
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 1, 0].passability = true)
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 1, 0].stability < 3)
     
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 2, 0].passability = true)
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 2, 0].stability < 3)   
      
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 3, 0].passability = true)
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 3, 0].stability < 3)
      
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 1, 0].passability = true)
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 1, 0].stability < 3)
    
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 2, 0].passability = true)
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 2, 0].stability < 3)   
     
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 3, 0].passability = true)
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 3, 0].stability < 3)
      //������ ���� �������
      and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] + 1, 0].passability = true)
      and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] + 1, 0].stability < 3)
     
      and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] + 2, 0].passability = true)
      and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] + 2, 0].stability < 3)   
      
      and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] + 3, 0].passability = true)
      and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] + 3, 0].stability < 3)
      //��������
      and (world[player_coordinate[1, 1] + 2, player_coordinate[1, 0] + 1, 0].passability = true)
      and (world[player_coordinate[1, 1] + 2, player_coordinate[1, 0] + 1, 0].stability < 3)
     
      and (world[player_coordinate[1, 1] + 2, player_coordinate[1, 0] + 2, 0].passability = true)
      and (world[player_coordinate[1, 1] + 2, player_coordinate[1, 0] + 2, 0].stability < 3))
      then 
      begin
        player_move(3, 1); 
        coordinate_correction_right;
        coordinate_correction_right;
        coordinate_correction_right;
        coordinate_correction_down;
        jump_was_bool_local := true; 
      end;
    end;
    
  end;
  
  
  
  jump_was_bool_local := false;
end;

procedure left_jump;
var
  jump_was_bool_local: boolean := false;
begin
  if ((player_coordinate[1, 0] > 1) and (player_coordinate[0, 1] > 1)) then 
  begin
    if ((world[player_coordinate[0, 1], player_coordinate[0, 0] - 1, 0].passability = false)
    and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 1, 0].stability >= 3)
    
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].stability < 3)
    and (world[player_coordinate[0, 1] - 2, player_coordinate[0, 0], 0].stability < 3)
    
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 1, 0].stability < 3)
    and (world[player_coordinate[0, 1] - 2, player_coordinate[0, 0] - 1, 0].stability < 3)
    
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].passability = true)
    and (world[player_coordinate[0, 1] - 2, player_coordinate[0, 0], 0].passability = true)
    
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 1, 0].passability = true)
    and (world[player_coordinate[0, 1] - 2, player_coordinate[0, 0] - 1, 0].passability = true))
    then 
    begin
      player_move(-1, -2); 
      coordinate_correction_left;
      coordinate_correction_up; 
      coordinate_correction_up; 
      jump_was_bool_local := true; 
    end;
  end;
  
  if ((player_coordinate[1, 0] > 2) and (player_coordinate[0, 1] > 0) and (jump_was_bool_local = false)) then 
  begin
    if ((world[player_coordinate[1, 1], player_coordinate[1, 0] - 2, 0].passability = false)
    and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 2, 0].stability >= 3)
    
    and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 1, 0].stability < 3)
    and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 1, 0].passability = true)
    
    and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 1, 0].stability < 3)
    and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 1, 0].passability = true)
    
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 1, 0].stability < 3)
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 1, 0].passability = true)
    
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].stability < 3)
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].passability = true)
    
    and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 2, 0].stability < 3)
    and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 2, 0].passability = true)
    
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 2, 0].stability < 3)
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 2, 0].passability = true))
    then 
    begin
      player_move(-2, -1); 
      coordinate_correction_left;
      coordinate_correction_left;
      coordinate_correction_up; 
      jump_was_bool_local := true; 
    end;
  end;
  
  if ((player_coordinate[1, 0] > 3) 
  and (player_coordinate[0, 1] > 0) 
  and (jump_was_bool_local = false)
  and (player_coordinate[1, 1] < length(world, 0) - 1 )) then
  begin
    if ((world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] - 3, 0].passability = false)
    and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] - 3, 0].stability >= 3)
    //
    and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] - 2, 0].stability < 3)
    and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] - 2, 0].passability = true)
    
    and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] - 1, 0].stability < 3)
    and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] - 1, 0].passability = true)
    //
    and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 3, 0].stability < 3)
    and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 3, 0].passability = true)
    
    and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 3, 0].stability < 3)
    and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 3, 0].passability = true)
    //
    and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 2, 0].stability < 3)
    and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 2, 0].passability = true)
    
    and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 2, 0].stability < 3)
    and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 2, 0].passability = true)
    //
    and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 1, 0].stability < 3)
    and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 1, 0].passability = true)
    
    and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 1, 0].stability < 3)
    and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 1, 0].passability = true)
    //
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].stability < 3)
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].passability = true)
    
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 1, 0].stability < 3)
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 1, 0].passability = true)
    
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 2, 0].stability < 3)
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 2, 0].passability = true)
    
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 3, 0].stability < 3)
    and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 3, 0].passability = true))
    then 
    begin
      player_move(-3, 0); 
      coordinate_correction_left;
      coordinate_correction_left;
      coordinate_correction_left;
      jump_was_bool_local := true; 
    end;
    
    if ((player_coordinate[1, 0] > 2) and (player_coordinate[0, 1] > 0) and (jump_was_bool_local = false)) then 
    begin
      if ((world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] - 2, 0].passability = false)
      and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] - 2, 0].stability >= 3)
      //
      and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] - 1, 0].stability < 3)
      and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] - 1, 0].passability = true)
      //
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 2, 0].stability < 3)
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 2, 0].passability = true)
      
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 2, 0].stability < 3)
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 2, 0].passability = true)
      //
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 1, 0].stability < 3)
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 1, 0].passability = true)
      
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 1, 0].stability < 3)
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 1, 0].passability = true)
      //
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].stability < 3)
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].passability = true)
      
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 1, 0].stability < 3)
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 1, 0].passability = true)
      
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 2, 0].stability < 3)
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 2, 0].passability = true))
      then 
      begin
        player_move(-2, 0); 
        coordinate_correction_left;
        coordinate_correction_left;
        jump_was_bool_local := true; 
      end;
    end;
    
    if ((player_coordinate[1, 0] > 2) and (player_coordinate[1, 1] < length(world, 0) - 2) 
    and (player_coordinate[0, 1] > 0) and (jump_was_bool_local = false)) then
    begin//������ ���� ������ ����������� ����� �� ��� ������ �, �� ����.
      if ((world[player_coordinate[1, 1] + 2, player_coordinate[1, 0] - 2, 0].passability = false)
      and (world[player_coordinate[1, 1] + 2, player_coordinate[1, 0] - 2, 0].stability >= 3)
      //
      and (world[player_coordinate[1, 1] + 2, player_coordinate[1, 0] - 1, 0].passability = true)
      and (world[player_coordinate[1, 1] + 2, player_coordinate[1, 0] - 1, 0].stability < 3)
     
      and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] - 1, 0].stability < 3)
      and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] - 1, 0].passability = true)
      //
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 2, 0].stability < 3)
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 2, 0].passability = true)
      
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 2, 0].stability < 3)
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 2, 0].passability = true)
      //
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 1, 0].stability < 3)
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 1, 0].passability = true)
      
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 1, 0].stability < 3)
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 1, 0].passability = true)
      //
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].stability < 3)
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].passability = true)
      
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 1, 0].stability < 3)
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 1, 0].passability = true)
      
      and (world[player_coordinate[0, 1] + 1, player_coordinate[0, 0] - 2, 0].stability < 3)
      and (world[player_coordinate[0, 1] + 1, player_coordinate[0, 0] - 2, 0].passability = true)
      
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 2, 0].stability < 3)
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 2, 0].passability = true))
      then 
      begin
        player_move(-2, 1); 
        coordinate_correction_left;
        coordinate_correction_left;
        coordinate_correction_down;
        jump_was_bool_local := true; 
      end;
    end;
    
    if ((player_coordinate[1, 0] > 2) and (player_coordinate[1, 1] < length(world, 0) - 2) 
    and (player_coordinate[0, 1] > 0) and (jump_was_bool_local = false)) then
    begin//������ ���� ������ ����������� ����� �� ��� ������ �, �� ����.
      if ((world[player_coordinate[1, 1] + 3, player_coordinate[1, 0] - 2, 0].passability = false)
      and (world[player_coordinate[1, 1] + 3, player_coordinate[1, 0] - 2, 0].stability >= 3)
      //
      and (world[player_coordinate[1, 1] + 2, player_coordinate[1, 0] - 1, 0].passability = true)
      and (world[player_coordinate[1, 1] + 2, player_coordinate[1, 0] - 1, 0].stability < 3)
     
      and (world[player_coordinate[1, 1] + 3, player_coordinate[1, 0] - 1, 0].passability = true)
      and (world[player_coordinate[1, 1] + 3, player_coordinate[1, 0] - 1, 0].stability < 3)   
      
      and (world[player_coordinate[1, 1] + 2, player_coordinate[1, 0] - 2, 0].passability = true)
      and (world[player_coordinate[1, 1] + 2, player_coordinate[1, 0] - 2, 0].stability < 3)
     
      and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] - 1, 0].stability < 3)
      and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] - 1, 0].passability = true)
      //
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 2, 0].stability < 3)
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 2, 0].passability = true)
      
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 2, 0].stability < 3)
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 2, 0].passability = true)
      //
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 1, 0].stability < 3)
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 1, 0].passability = true)
      
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 1, 0].stability < 3)
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 1, 0].passability = true)
      //
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].stability < 3)
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].passability = true)
      
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 1, 0].stability < 3)
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 1, 0].passability = true)
      
      and (world[player_coordinate[0, 1] + 1, player_coordinate[0, 0] - 2, 0].stability < 3)
      and (world[player_coordinate[0, 1] + 1, player_coordinate[0, 0] - 2, 0].passability = true)
      
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 2, 0].stability < 3)
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 2, 0].passability = true))
      then 
      begin
        player_move(-2, 2); 
        coordinate_correction_left;
        coordinate_correction_left;
        coordinate_correction_down;
        coordinate_correction_down;
        jump_was_bool_local := true; 
      end;
    end;
    
    if ((player_coordinate[1, 0] > 2) and (player_coordinate[1, 1] < length(world, 0) - 2) 
    and (player_coordinate[0, 1] > 0) and (jump_was_bool_local = false)) then
    begin//������ ���� ������ ����������� ����� �� ��� ������ �, �� ����.
      if ((world[player_coordinate[1, 1] + 2, player_coordinate[1, 0] - 3, 0].passability = false)
      and (world[player_coordinate[1, 1] + 2, player_coordinate[1, 0] - 3, 0].stability >= 3)//
      //�������
      and (world[player_coordinate[1, 1] - 1, player_coordinate[1, 0] - 1, 0].passability = true)
      and (world[player_coordinate[1, 1] - 1, player_coordinate[1, 0] - 1, 0].stability < 3)
     
      and (world[player_coordinate[1, 1] - 1, player_coordinate[1, 0] - 2, 0].passability = true)
      and (world[player_coordinate[1, 1] - 1, player_coordinate[1, 0] - 2, 0].stability < 3)   
      
      and (world[player_coordinate[1, 1] - 1, player_coordinate[1, 0] - 3, 0].passability = true)
      and (world[player_coordinate[1, 1] - 1, player_coordinate[1, 0] - 3, 0].stability < 3)
      //������ �� �������
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 1, 0].passability = true)
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 1, 0].stability < 3)
     
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 2, 0].passability = true)
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 2, 0].stability < 3)   
      
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 3, 0].passability = true)
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 3, 0].stability < 3)
      
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 1, 0].passability = true)
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 1, 0].stability < 3)
    
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 2, 0].passability = true)
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 2, 0].stability < 3)   
     
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 3, 0].passability = true)
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 3, 0].stability < 3)
      //������ ���� �������
      and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] - 1, 0].passability = true)
      and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] - 1, 0].stability < 3)
     
      and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] - 2, 0].passability = true)
      and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] - 2, 0].stability < 3)   
      
      and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] - 3, 0].passability = true)
      and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0] - 3, 0].stability < 3)
      //��������
      and (world[player_coordinate[1, 1] + 2, player_coordinate[1, 0] - 1, 0].passability = true)
      and (world[player_coordinate[1, 1] + 2, player_coordinate[1, 0] - 1, 0].stability < 3)
     
      and (world[player_coordinate[1, 1] + 2, player_coordinate[1, 0] - 2, 0].passability = true)
      and (world[player_coordinate[1, 1] + 2, player_coordinate[1, 0] - 2, 0].stability < 3))
      then 
      begin
        player_move(-3, 1); 
        coordinate_correction_left;
        coordinate_correction_left;
        coordinate_correction_left;
        coordinate_correction_down;
        jump_was_bool_local := true; 
      end;
    end;
    
  end;
end;


procedure climbing_up;
begin
  if player_coordinate[0, 1] > 0 then 
    if ((world[player_coordinate[0, 1] - 1, player_coordinate[1, 0], 0].passability = true)
    and (world[player_coordinate[0, 1] - 1, player_coordinate[1, 0], 0].stability < 3))
    then
    begin player_move(0, -1);coordinate_correction_up; end;
end;

procedure right_move;
begin
  if player_coordinate[1, 0] < length(world, 1) - 1 then 
  begin
    if ((world[player_coordinate[1, 1], player_coordinate[1, 0] + 1, 0].passability = true) 
    and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 1, 0].passability = true)
    
    and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 1, 0].stability < 4)
    and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 1, 0].stability < 4))
    then 
    begin player_move(1, 0);coordinate_correction_right; end else if auto_jump = true then
      
      if ((world[player_coordinate[1, 1], player_coordinate[1, 0] + 1, 0].passability = false)
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].passability = true) 
      
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 1, 0].passability = true)
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 1, 0].passability = true)
      
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] + 1, 0].stability < 3)
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 1, 0].stability < 3)
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].stability < 4)
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] + 1, 0].stability >= 3))
      then 
      begin player_move(1, -1);coordinate_correction_right;coordinate_correction_up; end;
    
  end;
end;

procedure left_move;
begin
  if player_coordinate[1, 0] > 0 then 
  begin
    if ((world[player_coordinate[1, 1], player_coordinate[1, 0] - 1, 0].passability = true) 
    and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 1, 0].passability = true)
    
    
    and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 1, 0].stability < 3)
    and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 1, 0].stability < 3))
    then 
    begin player_move(-1, 0);coordinate_correction_left; end else if auto_jump = true then
      
      if ((world[player_coordinate[1, 1], player_coordinate[1, 0] - 1, 0].passability = false) 
      
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 1, 0].passability = true)
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 1, 0].passability = true)
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].stability < 3)
      
      and (world[player_coordinate[0, 1], player_coordinate[0, 0] - 1, 0].stability < 3)
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 1, 0].stability < 3)
      and (world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].passability = true)
      and (world[player_coordinate[1, 1], player_coordinate[1, 0] - 1, 0].stability >= 3))
      then 
      begin player_move(-1, -1);coordinate_correction_left;coordinate_correction_up; end;
    
  end;
end;

procedure climbing_down;
begin
  if player_coordinate[1, 1] < length(world, 0) - 1  then 
    if ((world[player_coordinate[1, 1] + 1, player_coordinate[1, 0], 0].passability = true)
    and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0], 0].stability < 3))
    then
    begin player_move(0, 1);coordinate_correction_down; end;
end;

function set_block_color(symbol: string): byte;
begin
  for i: integer := 0 to length(types_of_blocks) - 1 do
    if types_of_blocks[i].symbol = symbol then result := types_of_blocks[i].color;
end;

procedure building(key: integer);
begin
  if ((player_coordinate[0, 1] > action_radius)
  and (player_coordinate[0, 0] > action_radius)
  and (player_coordinate[0, 1] < length(world, 0) - action_radius)
  and (player_coordinate[0, 0] < length(world, 1) - action_radius)) then
    case key of
      1:
        case action_radius of
          1:
            begin
              world[player_coordinate[0, 1] + 1, player_coordinate[0, 0] - 1, 0] := block_in_hand;
            end;
          2:
            begin
              world[player_coordinate[0, 1] + 2, player_coordinate[0, 0] - 1, 0] := block_in_hand;
            end;
        end;
      2:
        if (player_coordinate[0, 1] < length(world, 0) - 2) then
        begin
          world[player_coordinate[0, 1] + 2, player_coordinate[0, 0], 0] := block_in_hand;
        end;
      3:
        case action_radius of
          1:
            begin
              world[player_coordinate[0, 1] + 1, player_coordinate[0, 0] + 1, 0] := block_in_hand;
            end;
          2:
            begin
              world[player_coordinate[0, 1] + 2, player_coordinate[0, 0] + 1, 0] := block_in_hand;
            end;
        end;
      4:
        case action_radius of
          1:
            begin
              world[player_coordinate[0, 1], player_coordinate[0, 0] - 1, 0] := block_in_hand;
            end;
          2:
            case branch_hand_mode of
              'top':
                begin
                  world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 2, 0] := block_in_hand;
                end;
              'middle':
                begin
                  world[player_coordinate[0, 1], player_coordinate[0, 0] - 2, 0] := block_in_hand;
                end; 
              'bottom':
                begin
                  world[player_coordinate[0, 1] + 1, player_coordinate[0, 0] - 2, 0] := block_in_hand;
                end;
            end;
        end;
      6:
        case action_radius of
          1:
            begin
              world[player_coordinate[0, 1], player_coordinate[0, 0] + 1, 0] := block_in_hand;
            end;
          2:
            case branch_hand_mode of
              'top':
                begin
                  world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 2, 0] := block_in_hand;
                end;
              'middle':
                begin
                  world[player_coordinate[0, 1], player_coordinate[0, 0] + 2, 0] := block_in_hand;
                end; 
              'bottom':
                begin
                  world[player_coordinate[0, 1] + 1, player_coordinate[0, 0] + 2, 0] := block_in_hand;
                end;
            end;
        end;
      7:
        case action_radius of
          1:
            begin
              world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 1, 0] := block_in_hand;
            end;
          2:
            begin
              world[player_coordinate[0, 1] - 2, player_coordinate[0, 0] - 1, 0] := block_in_hand;
            end;
        end;
      8: 
        case action_radius of
          1:
            begin
              world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0] := block_in_hand;
            end;
          2: 
            begin
              world[player_coordinate[0, 1] - 2, player_coordinate[0, 0], 0] := block_in_hand;
            end;
        end;
      9:
        case action_radius of
          1:
            begin
              world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 1, 0] := block_in_hand;
            end;
          2:
            begin
              world[player_coordinate[0, 1] - 2, player_coordinate[0, 0] + 1, 0] := block_in_hand;
            end;
        end;
    end;
  
end;

procedure destroing(key: integer);
begin
  if ((player_coordinate[0, 1] > action_radius)
  and (player_coordinate[0, 0] > action_radius)
  and (player_coordinate[0, 1] < length(world, 0) - action_radius)
  and (player_coordinate[0, 0] < length(world, 1) - action_radius)) then
    case key of
      1:
        case action_radius of
          1:
            begin
              if world[player_coordinate[0, 1] + 1, player_coordinate[0, 0] - 1, 0].health - 1 > 0 then
                world[player_coordinate[0, 1] + 1, player_coordinate[0, 0] - 1, 0].health -= 1 else
                world[player_coordinate[0, 1] + 1, player_coordinate[0, 0] - 1, 0]:= types_of_blocks[7];
            end;
          2:
            begin
              if world[player_coordinate[0, 1] + 2, player_coordinate[0, 0] - 1, 0].health - 1 > 0 then
                world[player_coordinate[0, 1] + 2, player_coordinate[0, 0] - 1, 0].health -= 1 else
                world[player_coordinate[0, 1] + 2, player_coordinate[0, 0] - 1, 0]:= types_of_blocks[7];
            end;
        end;
      2:
        if (player_coordinate[0, 1] < length(world, 0) - 2) then
        begin
          if world[player_coordinate[0, 1] + 2, player_coordinate[0, 0], 0].health - 1 > 0 then
            world[player_coordinate[0, 1] + 2, player_coordinate[0, 0], 0].health -= 1 else
            world[player_coordinate[0, 1] + 2, player_coordinate[0, 0], 0]:= types_of_blocks[7];
        end;
      3:
        case action_radius of
          1:
            begin
              if world[player_coordinate[0, 1] + 1, player_coordinate[0, 0] + 1, 0].health - 1 > 0 then
                world[player_coordinate[0, 1] + 1, player_coordinate[0, 0] + 1, 0].health -= 1 else
                world[player_coordinate[0, 1] + 1, player_coordinate[0, 0] + 1, 0]:= types_of_blocks[7];
            end;
          2:
            begin
              if world[player_coordinate[0, 1] + 2, player_coordinate[0, 0] + 1, 0].health - 1 > 0 then
                world[player_coordinate[0, 1] + 2, player_coordinate[0, 0] + 1, 0].health -= 1 else
                world[player_coordinate[0, 1] + 2, player_coordinate[0, 0] + 1, 0]:= types_of_blocks[7];
            end;
        end;
      4:
        case action_radius of
          1:
            begin
              if world[player_coordinate[0, 1], player_coordinate[0, 0] - 1, 0].health - 1 > 0 then
                world[player_coordinate[0, 1], player_coordinate[0, 0] - 1, 0].health -= 1 else
                world[player_coordinate[0, 1], player_coordinate[0, 0] - 1, 0]:= types_of_blocks[7];
            end;
          2:
            case branch_hand_mode of
              'top':
                begin
                  if world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 2, 0].health - 1 > 0 then
                    world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 2, 0].health -= 1 else
                    world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 2, 0]:= types_of_blocks[7];
                end;
              'middle':
                begin
                  if world[player_coordinate[0, 1], player_coordinate[0, 0] - 2, 0].health - 1 > 0 then
                    world[player_coordinate[0, 1], player_coordinate[0, 0] - 2, 0].health -= 1 else
                    world[player_coordinate[0, 1], player_coordinate[0, 0] - 2, 0]:= types_of_blocks[7];
                end; 
              'bottom':
                begin
                  if world[player_coordinate[0, 1] + 1, player_coordinate[0, 0] - 2, 0].health - 1 > 0 then
                    world[player_coordinate[0, 1] + 1, player_coordinate[0, 0] - 2, 0].health -= 1 else
                    world[player_coordinate[0, 1] + 1, player_coordinate[0, 0] - 2, 0]:= types_of_blocks[7];
                end;
            end;
        end;
      6:
        case action_radius of
          1:
            begin
              if world[player_coordinate[0, 1], player_coordinate[0, 0] + 1, 0].health - 1 > 0 then
                world[player_coordinate[0, 1], player_coordinate[0, 0] + 1, 0].health -= 1 else
                world[player_coordinate[0, 1], player_coordinate[0, 0] + 1, 0]:= types_of_blocks[7];
            end;
          2:
            case branch_hand_mode of
              'top':
                begin
                  if world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 2, 0].health - 1 > 0 then
                    world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 2, 0].health -= 1 else
                    world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] + 2, 0]:= types_of_blocks[7];
                end;
              'middle':
                begin
                  if world[player_coordinate[0, 1], player_coordinate[0, 0] + 2, 0].health - 1 > 0 then
                    world[player_coordinate[0, 1], player_coordinate[0, 0] + 2, 0].health -= 1 else
                    world[player_coordinate[0, 1], player_coordinate[0, 0] + 2, 0]:= types_of_blocks[7];
                end; 
              'bottom':
                begin
                  if world[player_coordinate[0, 1] + 1, player_coordinate[0, 0] + 2, 0].health - 1 > 0 then
                    world[player_coordinate[0, 1] + 1, player_coordinate[0, 0] + 2, 0].health -= 1 else
                    world[player_coordinate[0, 1] + 1, player_coordinate[0, 0] + 2, 0]:= types_of_blocks[7];
                end;
            end;
        end;
      7:
        case action_radius of
          1:
            begin
              if world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 1, 0].health - 1 > 0 then
                world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 1, 0].health -= 1 else
                world[player_coordinate[0, 1] - 1, player_coordinate[0, 0] - 1, 0]:= types_of_blocks[7];
            end;
          2:
            begin
              if world[player_coordinate[0, 1] - 2, player_coordinate[0, 0] - 1, 0].health - 1 > 0 then
                world[player_coordinate[0, 1] - 2, player_coordinate[0, 0] - 1, 0].health -= 1 else
                world[player_coordinate[0, 1] - 2, player_coordinate[0, 0] - 1, 0]:= types_of_blocks[7];
            end;
        end;
      8: 
        case action_radius of
          1:
            begin
              if world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].health - 1 > 0 then
                world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0].health -= 1 else
                world[player_coordinate[0, 1] - 1, player_coordinate[0, 0], 0]:= types_of_blocks[7];
            end;
          2: 
            begin
              if world[player_coordinate[0, 1] - 2, player_coordinate[0, 0], 0].health - 1 > 0 then
                world[player_coordinate[0, 1] - 2, player_coordinate[0, 0], 0].health -= 1 else
                world[player_coordinate[0, 1] - 2, player_coordinate[0, 0], 0]:= types_of_blocks[7];
            end;
        end;
      9:
        case action_radius of
          1:
            begin
              if world[player_coordinate[0, 1] - 1, player_coordinate[0, 0]+1, 0].health - 1 > 0 then
                world[player_coordinate[0, 1] - 1, player_coordinate[0, 0]+1, 0].health -= 1 else
                world[player_coordinate[0, 1] - 1, player_coordinate[0, 0]+1, 0]:= types_of_blocks[7];
            end;
          2:
            begin
              if world[player_coordinate[0, 1] - 2, player_coordinate[0, 0]+1, 0].health - 1 > 0 then
                world[player_coordinate[0, 1] - 2, player_coordinate[0, 0]+1, 0].health -= 1 else
                world[player_coordinate[0, 1] - 2, player_coordinate[0, 0]+1, 0]:= types_of_blocks[7];
            end;
        end;
    end;
  
end;

procedure player_physics;
begin
  while ((player_coordinate[1, 1] < length(world, 0) - 1) and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0], 0].passability = true)
    and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0], 0].stability < 1)
    and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0], 0].condition < 1)) do
  begin
    player_move(0, 1);
    coordinate_correction_down;
  end;
  
  if ((player_coordinate[1, 1] < length(world, 0) - 1) and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0], 0].stability < 4)
    and (world[player_coordinate[1, 1] + 1, player_coordinate[1, 0], 0].stability > world[player_coordinate[1, 1], player_coordinate[1, 0], 0].stability )) then
  begin
    player_move(0, 1);
    coordinate_correction_down;
  end;
end;

procedure coordinate_correction_up;
begin
  if (height_print div 2 + 1) < length(world, 0) - player_coordinate[0, 1] then if corrective_height > 0 then corrective_height -= 1;  
end;

procedure coordinate_correction_down;
begin
  if height_print div 2 + 1 < player_coordinate[0, 1] then if (length(world, 0) - corrective_height) > height_print then corrective_height += 1; 
end;

procedure coordinate_correction_right;
begin
  if width_print div 2 + 1 < player_coordinate[0, 0] then if (length(world, 1) - corrective_width) > width_print then corrective_width += 1; 
end;

procedure coordinate_correction_left;
begin
  if (width_print div 2 + 1) < length(world, 1) - player_coordinate[0, 0] then if corrective_width > 0 then corrective_width -= 1;
end;

procedure player_move(x_delta, y_delta: integer);
begin
  player_coordinate[0, 0] += x_delta;
  player_coordinate[0, 1] += y_delta;
  
  player_coordinate[1, 0] := player_coordinate[0, 0];
  player_coordinate[1, 1] := player_coordinate[0, 1] + 1;
  //player_coordinate[1, 2] := player_coordinate[0, 2]; //this is Z axis
end;

procedure inventory;
var
  key: char;
begin
  gotoxy(width_print * 2 - inventory_tape.Length * 2 - 12, height_print + 1);
  textcolor(7);
  textbackground(0);
  write('choose from:');gotoxy(1, 1);
  textcolor(0);
  textbackground(7);
  key := readkey;
  case key of
    '1': begin block_in_hand := inventory_tape[0]; end;
    '2': begin block_in_hand := inventory_tape[1]; end;
    '3': begin block_in_hand := inventory_tape[2]; end;
    '4': begin block_in_hand := inventory_tape[3]; end;
    '5': begin block_in_hand := inventory_tape[4]; end;
    '6': begin block_in_hand := inventory_tape[5]; end;
    '7': begin block_in_hand := inventory_tape[6]; end;
    '8': begin block_in_hand := inventory_tape[7]; end;
    '9': begin block_in_hand := inventory_tape[8]; end;
    '0': begin block_in_hand := inventory_tape[9]; end;
  end;
end;

procedure generation_map;
begin
  for x: integer := 0 to length(world, 0) - 1 do
    for y: integer := 0 to length(world, 1) - 1 do
      world[x, y, 0].symbol := types_of_blocks[4].symbol;
  
  for x: integer := 0 to length(world, 0) - 1 do
    for y: integer := 0 to length(world, 1) - 1 do
    begin
      case x of
        0..30: world[x, y, 0].symbol := types_of_blocks[4].symbol;
        31..42: if random(10) < 7 then world[x, y, 0].symbol := types_of_blocks[0].symbol else world[x, y, 0].symbol := types_of_blocks[2].symbol;
        43..44: world[x, y, 0].symbol := types_of_blocks[2].symbol;
        45..200: world[x, y, 0].symbol := types_of_blocks[3].symbol;
      end;
    end;
  
  for x: integer := 0 to length(world, 0) - 1 do
    for y: integer := 0 to length(world, 1) - 1 do
      for i: integer := 0 to length(types_of_blocks) - 1 do
      begin
        if types_of_blocks[i].symbol = world[x, y, 0].symbol then world[x, y, 0].passability := types_of_blocks[i].passability;
        if types_of_blocks[i].symbol = world[x, y, 0].symbol then world[x, y, 0].stability := types_of_blocks[i].stability;
      end;
  
  
end;

procedure walk(key: char);
begin
  case key of
    'q', '�': begin left_jump; end;
    'e', '�': begin right_jump; end;
    'w', '�': begin climbing_up; end;
    'a', '�': begin left_move; end;
    's', '�': begin climbing_down; end;
    'd', '�': begin right_move end;
  end;
end;

procedure preservation;
var
  map: text;
begin
  assign(map, '31.tcms');
  rewrite(map);
  writeln(map, player_coordinate[0, 1]:4, player_coordinate[0, 0]:4);
  
  for y: integer := 0 to length(world, 0) - 1 do 
  begin
    for x: integer := 0 to length(world, 1) - 1 do 
    begin
      write(map, world[y, x, 0].symbol);
    end;
    if y mod 25 = 0 then write('#'); end;
  
  close(map);
end;

procedure loading;
var
  map: text;
  s: string;
  i: integer;
begin
  assign(map, '31.tcms');
  reset(map);
  readln(map, s);
  player_coordinate[0, 1] := strtoint(s[1] + s[2] + s[3] + s[4]);
  player_coordinate[0, 0] := strtoint(s[5] + s[6] + s[7] + s[8]);
  player_move(0, 0);
  close(map);
  reset(map);
  readln(map, s);
  read(map, s);
  close(map);
  i := 1;
  for y: integer := 0 to length(world, 0) - 1 do 
  begin
    for x: integer := 0 to length(world, 1) - 1 do 
    begin
      world[y, x, 0].symbol := s[i];
      for j: integer := 0 to length(types_of_blocks) - 1 do
      begin
        if types_of_blocks[j].symbol = world[y, x, 0].symbol then 
        begin
          world[y, x, 0] := types_of_blocks[j];
        end;
      end;
      i += 1;
    end;
    if y mod 25 = 0 then write('#');
  end;
end;

procedure block_physics;
var
  bufer: block;
begin
  for y: integer := player_coordinate[0, 1] - width_print * 1 to player_coordinate[0, 1] + width_print * 1 do 
  begin
    for x: integer := player_coordinate[0, 0] - height_print * 1 to player_coordinate[0, 0] + height_print * 1 do 
    begin
      if world[y, x, 0] = types_of_blocks[1] then
        if ((world[y + 1, x, 0] = types_of_blocks[4]) or (world[y + 1, x, 0] = types_of_blocks[0])) then 
        begin bufer := world[y, x, 0];world[y, x, 0] := world[y + 1, x, 0];world[y + 1, x, 0] := bufer; end;
      
      if world[y, x, 0] = types_of_blocks[1] then
        if (((world[y + 1, x + 1, 0] = types_of_blocks[4]) or (world[y + 1, x + 1, 0] = types_of_blocks[0])) and 
        ((world[y + 1, x - 1, 0] = types_of_blocks[4]) or (world[y + 1, x - 1, 0] = types_of_blocks[0]))) then 
          if random(2) = 1 then
          begin
            bufer := world[y, x, 0];world[y, x, 0] := world[y + 1, x + 1, 0];world[y + 1, x + 1, 0] := bufer;
          end else
          begin
            bufer := world[y, x, 0];world[y, x, 0] := world[y + 1, x - 1, 0];world[y + 1, x - 1, 0] := bufer;
          end else
        if world[y, x, 0] = types_of_blocks[1] then
          if ((world[y + 1, x + 1, 0] = types_of_blocks[4]) or (world[y + 1, x + 1, 0] = types_of_blocks[0])) then
          begin
            bufer := world[y, x, 0];world[y, x, 0] := world[y + 1, x + 1, 0];world[y + 1, x + 1, 0] := bufer;
          end else
          if ((world[y + 1, x - 1, 0] = types_of_blocks[4]) or (world[y + 1, x - 1, 0] = types_of_blocks[0])) then
          begin
            bufer := world[y, x, 0];world[y, x, 0] := world[y + 1, x - 1, 0];world[y + 1, x - 1, 0] := bufer;
          end;
      
      if world[y, x, 0] = types_of_blocks[0] then
        if world[y + 1, x, 0] = types_of_blocks[4] then 
        begin bufer := world[y, x, 0];world[y, x, 0] := world[y + 1, x, 0];world[y + 1, x, 0] := bufer; end;
      
      var i: integer := 1;
      
      if world[y, x, 0] = types_of_blocks[0] then
        if world[y + 1, x, 0] = types_of_blocks[0] then
          while world[y + 1, x + i, 0] = types_of_blocks[0] do 
          begin
            i += 1;
          end;
      if ((world[y + 1, x + i, 0] = types_of_blocks[4]) and (i > 1)) then 
      begin world[y + 1, x + i, 0] := types_of_blocks[0];world[y, x, 0] := types_of_blocks[4]; end;
      
      i := -1;
      
      if world[y, x, 0] = types_of_blocks[0] then
        if world[y + 1, x, 0] = types_of_blocks[0] then
          while world[y + 1, x + i, 0] = types_of_blocks[0] do 
          begin
            i -= 1;
          end;
      if ((world[y + 1, x + i, 0] = types_of_blocks[4]) and (i < -1)) then 
      begin world[y + 1, x + i, 0] := types_of_blocks[0];world[y, x, 0] := types_of_blocks[4]; end;
      
      if world[y, x, 0] = types_of_blocks[0] then if world[y, x + 1, 0] = types_of_blocks[4] then if world[y, x - 1, 0] = types_of_blocks[4]
          then begin world[y, x + 1, 0] := types_of_blocks[0];world[y, x, 0] := types_of_blocks[4]; end;
      
      if world[y, x, 0] = types_of_blocks[0] then if world[y, x + 1, 0] = types_of_blocks[4] then 
        begin world[y, x + 1, 0] := types_of_blocks[0];world[y, x, 0] := types_of_blocks[4]; end;
      
      if world[y, x, 0] = types_of_blocks[0] then if world[y, x - 1, 0] = types_of_blocks[4] then 
        begin world[y, x - 1, 0] := types_of_blocks[0];world[y, x, 0] := types_of_blocks[4]; end;
      
      if world[y,x,0].symbol = types_of_blocks[7].symbol_drop then world[y,x,0] := types_of_blocks[7];
    end;
    
  end;
  
end;

end.